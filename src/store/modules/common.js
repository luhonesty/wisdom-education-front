export default {
  namespaced: true,
  state: {
    videoUrl: null,
    testPaperInfo: null,
    host: 'http://localhost',
    fileHost: 'http://localhost/uploads',
    webSocketHost: 'localhost',
    proxyHttp: true // 是否开启http代理
  },

  mutations: {

    updateTestPaperInfo (state, testPaperInfo) {
      state.testPaperInfo = testPaperInfo
      sessionStorage.setItem('testPaperInfo', JSON.stringify(testPaperInfo))
    },

    updateVideoUrl (state, data) {
      state.videoUrl = data
      sessionStorage.setItem('videoUrl', JSON.stringify(data))
    }

  },

  getters: {

    getTestPaperInfo (state) {
      if (state.testPaperInfo != null) {
        return state.testPaperInfo
      }
      return JSON.parse(sessionStorage.getItem('testPaperInfo'))
    },

    getVideoUrl (state) {
      if (state.videoUrl) {
        return state.videoUrl
      }
      return JSON.parse(sessionStorage.getItem('videoUrl'))
    }
  }
}
